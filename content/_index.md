---
title: Home
---

![zeta](/static/nodo.png)

# NODO AMBIENTAL _zeta_

## _Información y nexos_
**Para la preservación del ambiente y el desarrollo sostenible**

**Nodo Ambiental** es una organización que colabora con el manejo de información y con redes de cooperación para la preservación del ambiente y el desarrollo sostenible.

**Nodo Ambiental** es una Organización de la Sociedad Civil (OSC), no gubernamental (ONG), dirigida a otras organizaciones, escuelas y profesionales.

## _Buscamos_
1 Colaborar en el desarrollo y ejecución de proyectos de pertinencia social y ambiental.

2 Difundir proyectos de salud ambiental, incluyendo aspectos sociales, educativos y lúdicos.

3 Desarrollar repositorios de información verificable.


### Nuestra Visión
Los problemas de salud humana y de bienestar social están embebidos en problemas de conservación de la naturaleza y del manejo de recursos naturales. La salud humana y ambiental, la salud de los servicios que brinda la naturaleza _-recursos naturales comunes o compartidos (aire, agua, suelo) que nos llegan como beneficios espontáneos de la naturaleza-_, son afectados por actividades humanas. El impacto individual de las actividades humanas parece bajo, pero sumados tienen efectos muy importantes sobre el bienestar social, la salud ambiental y humana. 

Es por ello que la conjunción de mecanismos de articulación, desarrollo humano, desarrollo tecnológico, herramientas de cooperación entre organizaciones, espacios educativos, artísticos y multimedios, ayudan a encontrar soluciones prácticas en _entornos donde el tiempo es limitado_. Además, la verificación de la información difundida es clave para establecer bases sólidas para la realización y alcance de metas. 

El desarrollo de nodos o sistemas de cooperación y articulación a distancia es una herramienta sólida para ayudar a que los proyectos de educación ambiental, conservación de la naturaleza y salud humana y ambiental, logren sus metas. Entre ellas, la reducción del impacto humano sobre el ambiente en que vivimos.

### Nuestra Misión
Desarrollar sistemas de cooperación entre Organizaciones de la Sociedad Civil (ONG), escuelas y profesionales, para la verificación de información y para la difusión, desarrollo y ejecución de proyectos de pertinencia social y ambiental.

### Cómo trabajamos
Proponemos un esquema de trabajo mutuamente verificable. A un primer proceso de contacto y formulación de objetivos, prosigue un esquema de interacción que termina con el alcance de metas.

### Lemas
_La conservación de la naturaleza es nuestra salud_

**Nodo Ambiental _zeta_**: estamos en período de prueba.
Escriba a correonodoambiental@gmail.com si considera colaborar con nosotros.
Le invitamos a poner a prueba nuestros [servicios](/servicios/).

La sección [Acerca de](/acerca/) incluye los términos y condiciones del uso de datos.
