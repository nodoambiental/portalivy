---
title: Acerca de Nodo Ambiental
---

Portal desarrollado mediante blogdown [https://bookdown.org/yihui/blogdown/], RStudio Version 1.1.456 [https://www.rstudio.com/], R Version 3.5.1 [https://www.r-project.org/], repositorios GitHub [https://github.com/nodoambiental/] y GitLab [https://gitlab.com/nodoambiental/], dominio vía donweb [https://donweb.com/es-ar/], hosting y certificados en netlify [https://app.netlify.com/].

# _Términos y condiciones_
Información para difusión disponible bajo licencia MIT [https://opensource.org/licenses/MIT] y Creative Commons [https://creativecommons.org/].

# Directrices de privacidad
_nodoambiental.org_

**Cómo registramos, usamos, y compartimos información personal**
La información personal que usted brinda a través del registro en nuestro portal, o la que nos envía para la realización de servicios o actividades y relaciones específicas, queda en un repositorio seguro de nodoambiental [https://gitlab.com/nodoambiental].

No compartimos información personal de su registro en nodoambiental sin su consentimiento explícito (correo electrónico o formulario).

En el caso de que usted haya firmado con su nombre entrevistas, o haga comentarios en foros, o participe en la generación de contenidos vinculados a tareas de nodoambiental, su nombre, localidad geográfica y grupo de pertenencia, lo consideramos información pública, misma que usted podrá visualizar en los contenidos que nodoambiental haga públicos o comparta con usted.

Siempre podrá retirar información personal a resguardo en los repositorios, o restringir el uso de información que explícitamente nos comunique, con excepción de la información de su nombre, localidad, o grupo de pertinencia y actividades relacionadas a las tareas de nodoambiental que se consideren públicas.

La información de identificación personal (tales como documentos, pasaportes, u otra información de referencia fiscal o legal) que usted envía para nuestros registros será clasificada a resguardo seguro, en el caso de que usted voluntariamente nos envíe tal información.

La información sobre domicilio postal o de residencia no será pública. Dicha información será accedida solamente por miembros del consejo de nodoambiental, o sólo para usos específicos vinculados con eventos donde dicha información debe figurar y ser compartida con restricciones específicas, nunca de dominio público. 

Para la correspondencia o interacción directa en la web (chat, foros) usaremos su primer nombre, el cual usted podrá ver siempre.

**Cómo protegemos información personal**
En _nodoambiental_ ejecutamos y mantenemos medidas de seguridad y prácticas apropiadas para el registro, repositorio (almacenamiento) y procesamiento para la protección contra accesos no autorizados, alteraciones de información, revelación o destrucción de su información personal, nombre de usuario, contraseña, información de transacciones web y datos almacenados en nuestro sitio. 

**Cambios en estas directrices de privacidad**
Nodoambiental conserva el criterio de actualizar estas directrices de privacidad en cualquier momento. Cuando hacemos esto, publicamos la fecha de modificación al pie de esta página.

El reconocimiento y aceptación de las actualizaciones de estas directrices de privacidad queda bajo su responsabilidad, por lo que le sugerimos revisarlos periódicamente y estar al tanto de los cambios que realizamos. 

Le enviaremos actualizaciones sobre estos cambios y otros anuncios vía correo electrónico. Usted puede dejar de estar suscripto de nuestra lista de correos en cualquier momento que lo desee.

Si no desea recibir mensajes, por favor, escriba a correonodoambiental@gmail.com.
