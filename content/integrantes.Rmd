---
title: "Integrantes"
categories: ["R"]
tags: ["R Markdown"]
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(collapse = TRUE)
```

**Nodo Ambiental se forma como un equipo multidisciplinario de profesionales, docentes, técnicos y artistas**

# Quiénes somos

_Laura González_, Antropóloga, Dirección Ejecutiva

_Mariano Ordano_, Biólogo, Doctor en Ciencias en Ecología y Manejo de Recursos Naturales, Dirección Técnica

# Quiénes colaboraron

_Ágata Irene Ordano_, Técnica Electromecánica, Desarrollo web, diseño y desarrollo de ideas, flujos de trabajo.

